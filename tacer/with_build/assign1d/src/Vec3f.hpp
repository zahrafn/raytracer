#ifndef VEC3F_HPP
#define VEC3F_HPP

#include <sstream> // stringstreams
#include "math.h"
#include <ostream>
#include <iostream>



class Vec3f {
public:
    Vec3f() : x(0), y(0), z(0) { }

    Vec3f(float x_, float y_, float z_) : x(x_), y(y_), z(z_) { }

    double x, y, z;

    std::string to_str();


    void crossP(Vec3f &v, Vec3f &v_res) {

        v_res.x = this->y * v.z - this->z * v.y;
        v_res.y = this->z * v.x - this->x * v.z;
        v_res.z = this->x * v.y - this->y * v.x;


    }

//	void dotP(Vec3f &v , double res){
//		res = this->x * v.x + this->y * v.y + this->z * v.z;
//
//	}

    double dotP(Vec3f &v) {

        double res;
        res = this->x * v.x + this->y * v.y + this->z * v.z;
        return res;
    }

    void constP(double f, Vec3f &v_res) {

        v_res.x = f * this->x;
        v_res.y = f * this->y;
        v_res.z = f * this->z;


    }

    void add(Vec3f &v, Vec3f &v_res) {

        v_res.x = this->x + v.x;
        v_res.y = this->y + v.y;
        v_res.z = this->z + v.z;


    }

    void subtract(Vec3f &v, Vec3f &v_res) {

        v_res.x = this->x - v.x;
        v_res.y = this->y - v.y;
        v_res.z = this->z - v.z;


    }

    void normalize() {

        double mag_sq = this->x * this->x + this->y * this->y + this->z * this->z;
        double mag = sqrt(mag_sq);
        this->x = this->x / mag;
        this->y = this->y / mag;
        this->z = this->z / mag;
    }

    double length() {

        double mag_sq = this->x * this->x + this->y * this->y + this->z * this->z;
        double mag = sqrt(mag_sq);
        return mag;

    }



    void setVec(Vec3f &v) {

        this->x = v.x;
        this->y = v.y;
        this->z = v.z;

    }

    void print(std::string prefix){
        std::cout << prefix << this->x << " " << this->y<< " "<< this->z << std::endl;


    }




}; // end


#endif
