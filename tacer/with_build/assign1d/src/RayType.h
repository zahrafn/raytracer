//
// Created by Zahra Forootaninia on 9/21/16.
//

#ifndef EXAMPLE_RAYTYPE_H
#define EXAMPLE_RAYTYPE_H


#include "Vec3f.hpp"

class RayType {
public:
    float x, y, z;
    float dx, dy, dz;
    float eta;

    RayType() :x(0), y(0), z(0), dx(0), dy(0), dz(0) {}
    RayType( Vec3f origin_, Vec3f direction_) : x(origin_.x), y(origin_.y), z(origin_.z),
                                                dx(direction_.x), dy(direction_.y), dz(direction_.z) { }

    RayType( Vec3f origin_, Vec3f direction_, float eta_) : x(origin_.x), y(origin_.y), z(origin_.z),
                                                dx(direction_.x), dy(direction_.y), dz(direction_.z), eta(eta_) { }


};


#endif //EXAMPLE_RAYTYPE_H
