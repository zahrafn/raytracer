//
// Created by Zahra Forootaninia on 10/18/16.
//

#ifndef EXAMPLE_SHAPE_H
#define EXAMPLE_SHAPE_H

#include "Vertex.h"
#include "Color.h"
#include "Vec3f.hpp"
#include "RayType.h"
#include "math.h"
#include <math.h>       /* atan2 */
#include <iostream>


const float small = 1e-3;


struct hit{

    int id;
    float dist;
    Vec3f normal;
    float eta;
};

class Shape {

public:

    int id;

    virtual double area() = 0;

    virtual Material get_matrial() = 0;
    virtual std::vector< Color > get_texture() =0;

    virtual bool is_texture() = 0;

    virtual bool barycentric_cord(Vec3f &intersection_plane_point, double &alpha, double &beta,
                                  double &gamma) = 0;

    virtual Vec3f interpolate(double alpha, double beta, double gamma, Vec3f n0, Vec3f n1, Vec3f n2) = 0;

    virtual void intersect(RayType &ray, int inx, std::vector< hit > &intersect_shapes,
                           std::vector<std::pair<int, double> > &shape_sh) = 0;

    virtual void get_normal(Vec3f intersection_point, Vec3f &normal) = 0;

    virtual void texture_cord(Vec3f intersection_point,Vec3f &texture ) = 0;

    int get_id(){

        return id;
    }
    //virtual double get_dist(Triangle triangle);



};

class Triangle : public Shape {

public:
    Triangle() : f1(0), f2(0), f3(0), v1(), v2(), v3(), vt1(), vt2(), vt3(), n1(), n2(), n3(), textureflag(false) { }



//  constructor without normals
    Triangle(int f1_, int f2_, int f3_,
             Vec3f v1_, Vec3f v2_, Vec3f v3_,
             int id_) :

            f1(f1_), f2(f2_), f3(f3_),
            v1(v1_), v2(v2_), v3(v3_)//,
//            id(id_)



    {
        //std::cout << "constructor 1 no normals " << std::endl;

        // make normals for triangle
        Vec3f e1, e2;
        this->v2.subtract(this->v1, e1);
        this->v3.subtract(this->v1, e2);
        e1.crossP(e2, n1);
        n1.normalize();

        n2 = n1;
        n3 = n1;
        this->id = id_;
    }

    // have normals for triangle
    Triangle(int f1_, int f2_, int f3_,
             Vec3f v1_, Vec3f v2_, Vec3f v3_,
             Vec3f n1_, Vec3f n2_, Vec3f n3_,
              int id_) :

            f1(f1_), f2(f2_), f3(f3_),
            v1(v1_), v2(v2_), v3(v3_),
            n1(n1_), n2(n2_), n3(n3_){

        //std::cout << "constructor 5 have normals and texture " << std::endl;
        this->id = id_;

    }

    int f1, f2, f3;
    Vec3f v1, v2, v3;
    Vec3f vt1, vt2, vt3;
    Vec3f n1, n2, n3;
    bool textureflag;
    std::vector< Color > texture;

    Material mat;
    //int id;

//    int get_id(){
//        return id;
//    }

    Material get_matrial() {
        return mat;
    }

    void set_matrial( Material matiral_in){
        mat = matiral_in;
    }

    std::vector< Color > get_texture() {
        return texture;
    }

    void set_texture(const std::vector< Color > &texture_in){
        texture = texture_in;
    }

    bool is_texture() {
        return textureflag;
    }

    void set_texture_bool(bool istex){
        textureflag = istex;
    }

    void set_texture_vertices(Vec3f vt_in_1,Vec3f vt_in_2,Vec3f vt_in_3 ){

        vt1 = vt_in_1;
        vt2 = vt_in_2;
        vt3 = vt_in_3;

    }



    double area() {


        Vec3f e1, e2, cross_p;
        this->v2.subtract(this->v1, e1);
        this->v3.subtract(this->v1, e2);


        e1.crossP(e2, cross_p);

        double area = 0.5 * cross_p.length();

        return area;


    }


    bool barycentric_cord(Vec3f &intersection_plane_point, double &alpha, double &beta, double &gamma) {


        float epsilon = 0.001;
        double inside = false;

        double inv_area = 1./ this->area();

        Vec3f v = intersection_plane_point;

        // e1 = v2 - v1
        // e2 = v - v1
        Triangle triangle_a(this->f1, this->f2, 0, this->v2, this->v3, v, 0);

        // e1 = v - v1
        // e2 = v3 - v1
        Triangle triangle_b(this->f1, 0, this->f3, this->v1, v, this->v3, 0);
        // e1 = v2 - v
        // e2 = v3 - v
        Triangle triangle_c(0, this->f2, this->f3, v, this->v2, this->v1, 0);

        // get Barycentric cordinates

        alpha = triangle_a.area() * inv_area;
        beta = triangle_b.area() * inv_area;
        gamma = triangle_c.area() * inv_area;


        if (alpha + beta + gamma - 1 < epsilon) {
            inside = true;
        }


        return inside;


    }

    Vec3f interpolate(double alpha, double beta, double gamma, Vec3f n0, Vec3f n1, Vec3f n2) {

//                      const Vec3f& n0, const Vec3f& n1, const Vec3f& n2) {

        Vec3f seg1, seg2, seg3, sum_mid, sum, n;
        double mag_sum;

        n0.constP(alpha, seg1);
        n1.constP(beta, seg2);
        n2.constP(gamma, seg3);

        seg1.add(seg2, sum_mid);
        sum_mid.add(seg3, sum);

        mag_sum = sum.length();

        sum.constP(1. / mag_sum, n);

//        std::cout << "n : " << n.to_str() << std::endl ;

        return n;


    }

    void intersect(RayType &ray, int inx, std::vector< hit > &intersect_shapes,
                   std::vector<std::pair<int, double> > &shape_sh) {


        //std::cout << "v: " << this->v2.x << " " << this->v2.y<< " "<< this->v2.z << std::endl;


        // finding the triangle plane
        Vec3f e1, e2, nrm;
        this->v2.subtract(this->v1, e1);
        this->v3.subtract(this->v1, e2);
        e1.crossP(e2, nrm);
        nrm.normalize();


        double d = -(nrm.dotP(this->v1));
        Vec3f ray_dir = Vec3f(ray.dx, ray.dy, ray.dz);
        Vec3f ray_int = Vec3f(ray.x, ray.y, ray.z);
        double t_denominator = nrm.dotP(ray_dir);
        //std::cout << " t_denominator != 0  "<< t_denominator<<std::endl;


        if (t_denominator != 0) {


            double t = -(nrm.dotP(ray_int) + d) / t_denominator;


            if (t > small) {

                hit h;
                h.id = inx;
                h.dist = t;
                h.eta = this->get_matrial().refraction_index;
                //TODO add normals and eta

                Vec3f intersection_plane_point = Vec3f(ray.x + t * (ray.dx), ray.y + t * (ray.dy),
                                                       ray.z + t * (ray.dz));
                double alpha, beta, gamma;
                bool inside = barycentric_cord(intersection_plane_point, alpha, beta, gamma);

                if (inside) {

                    intersect_shapes.push_back( h );

                    if (t > this->area() * 2) {
                        shape_sh.push_back(std::make_pair(inx, t));
                    }
                    //shape_sh.push_back(std::make_pair(inx, t));}


                }
            }

        }


    }


    void get_normal(Vec3f intersection_point, Vec3f &normal) {

 //       double alpha, beta, gamma;


        double alpha = 0.;
        double beta = 0.;
        double gamma = 0.;
        bool inside = barycentric_cord(intersection_point, alpha, beta, gamma);


        normal = interpolate(alpha, beta, gamma, this->n1, this->n2, this->n3);
        normal.normalize();


        //}




    }

    void texture_cord(Vec3f intersection_point, Vec3f &texture_cordinate) {

        double sum_mag_vt;
        sum_mag_vt = this->vt1.length() + this->vt2.length() + this->vt3.length();


        if (sum_mag_vt > 0) {

            double alpha = 0.;
            double beta = 0.;
            double gamma = 0.;
            bool inside = barycentric_cord(intersection_point, alpha, beta, gamma);



            texture_cordinate = interpolate(alpha, beta, gamma, this->vt1, this->vt2, this->vt3);


//            textureU = alpha * this->vt1.x + beta * this->vt2.x + gamma * this->vt3.x;
//            textureV = alpha * this->vt1.y + beta * this->vt2.y + gamma * this->vt3.y;

//            double mag_tex_cord = textureU * textureU + textureV * textureV;
//            double mag = sqrt(mag_tex_cord);
//            textureU = textureU / mag;
//            textureU = textureU / mag;


//            std::cout << "alpha, beta, gamma:  " << alpha <<" "<<beta<< " "<< gamma << std::endl;
//            std::cout << "this->vt1.x, this->vt2.x,  this->vt3.x " << this->vt1.x  <<" "<< this->vt2.x << " "<< this->vt3.x  << std::endl;
//            std::cout << "this->vt1.y, this->vt2.y:, this->vt3.y " << this->vt1.y  <<" "<< this->vt2.y << " "<< this->vt3.y  << std::endl;
//
//            std::cout << "u, v shape:  " << textureU <<" "<<textureV<< " "<< std::endl;


        }

    }


};

class Sphere : public Shape {
public:
    Sphere() : x(0), y(0), z(0), r(0), texture(false), mat(), currtexture() { }

    Sphere(double x_, double y_, double z_, double r_, bool texture_, Material mat_,  std::vector< Color > currtexture_,
           int id_) : x(x_), y(y_), z(z_), r(r_), texture(texture_), mat(mat_), currtexture(currtexture_){

        this->id = id_;
    }

    Sphere(double x_, double y_, double z_, double r_, Material mat_, int id_) :
            x(x_), y(y_), z(z_), r(r_){

        this->id = id_;
    }

    double x, y, z, r;
    Material mat;
    std::vector< Color > currtexture;
    bool texture;
    //int id;

    Material get_matrial() {
        return mat;
    }

    std::vector< Color > get_texture() {
        return currtexture;
    }

    bool is_texture() {
        return texture;
    }

    double area() { return 0; }

    bool barycentric_cord(Vec3f &intersection_plane_point, double &alpha, double &beta,
                          double &gamma) { return 0; };

    Vec3f interpolate(double alpha, double beta, double gamma, Vec3f n0, Vec3f n1, Vec3f n2) {
        Vec3f v;
        return v;
    };


    void intersect(RayType &ray, int inx, std::vector< hit > &intersect_shapes,
                   std::vector<std::pair<int, double> > &shape_sh) {

        double alpha = (ray.dx * ray.dx + ray.dy * ray.dy + ray.dz * ray.dz);
        double beta = 2 * (ray.dx * (ray.x - this->x) + ray.dy * (ray.y - this->y) +
                           ray.dz * (ray.z - this->z));
        double gamma =
                (ray.x - this->x) * (ray.x - this->x) + (ray.y - this->y) * (ray.y - this->y) +
                (ray.z - this->z) * (ray.z - this->z) - (this->r * this->r);
        double discriminant = beta * beta - 4 * alpha * gamma;


        Vec3f intersection_point;
        Vec3f norm;
        hit intersect_hit;

        if (discriminant > 0) {
            double t_1 = (-beta + sqrt(discriminant)) / (2 * alpha);
            double t_2 = (-beta - sqrt(discriminant)) / (2 * alpha);

//            std::cout << "t_1 "<< std::setprecision(5)<< t_1 << std::endl;
//            std::cout << "t_2 "<< std::setprecision(5)<< t_2 << std::endl;


            if (t_1 > small) {


                intersection_point.x = ray.x + t_1 * ray.dx;
                intersection_point.y = ray.y + t_1 * ray.dy;
                intersection_point.z = ray.z + t_1 * ray.dz;
                this->get_normal(intersection_point, norm);

                intersect_hit.id = this->id;
                intersect_hit.dist = t_1;
                intersect_hit.eta = this->get_matrial().refraction_index;
                intersect_hit.normal = norm;


                intersect_shapes.push_back( intersect_hit );
                if (t_1 > this->r) {
                    shape_sh.push_back(std::make_pair(this->id, t_1));

                }


            }
            if (t_2 > small) {

                intersection_point.x = ray.x + t_2 * ray.dx;
                intersection_point.y = ray.y + t_2 * ray.dy;
                intersection_point.z = ray.z + t_2 * ray.dz;
                this->get_normal(intersection_point, norm);

                intersect_hit.id = this->id;
                intersect_hit.dist = t_2;
                intersect_hit.eta = this->get_matrial().refraction_index;
                intersect_hit.normal = norm;

                intersect_shapes.push_back( intersect_hit );
                if (t_2 > this->r) {
                    shape_sh.push_back(std::make_pair(this->id, t_2));
                }


            }
        }
        else if (discriminant == 0) {
            double t = -beta / (2 * alpha);

            if (t > small) {
                intersection_point.x = ray.x + t * ray.dx;
                intersection_point.y = ray.y + t * ray.dy;
                intersection_point.z = ray.z + t * ray.dz;
                this->get_normal(intersection_point, norm);

                intersect_hit.id = this->id;
                intersect_hit.dist = t;
                intersect_hit.eta = this->get_matrial().refraction_index;
                intersect_hit.normal = norm;

                intersect_shapes.push_back( intersect_hit );
                if (t > this->r) {
                    shape_sh.push_back(std::make_pair(this->id, t));
                }

            }

        }
    }

    void get_normal(Vec3f intersection_point, Vec3f &normal) {

        Vec3f sphere_cent;

        sphere_cent.x = this->x;
        sphere_cent.y = this->y;
        sphere_cent.z = this->z;

        //finding the sphere norm
        Vec3f res2;
        intersection_point.subtract(sphere_cent, res2);
        res2.constP(1. / this->r, normal);
        normal.normalize();
    }

    void texture_cord(Vec3f intersection_point, Vec3f &texture) {

        float  dist_zdir = intersection_point.z - this->z;
//        float z_dist;
        float phi = acos(dist_zdir / this->r);

        Vec3f normal;
        this->get_normal(intersection_point, normal);
        float theta = atan2(normal.y, normal.x);


        texture.x = (theta + M_PI)/(2. * M_PI);  // u


//        phi = phi + 0.25 * M_PI;
//
//        if(phi > M_PI) phi = phi - M_PI;

        texture.y = phi/ M_PI;                  //v




    }


};


#endif //EXAMPLE_SHAPE_H
