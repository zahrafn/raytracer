#include "Vec3f.hpp"

std::string Vec3f::to_str(){
	// We can also use stringstreams to cast numbers back into strings!
	std::stringstream ss;
	ss << x << ' ' << y <<' '<< z;
	return ss.str();
}


