//
// Created by Zahra Forootaninia on 10/3/16.
//

#ifndef EXAMPLE_LIGHT_H
#define EXAMPLE_LIGHT_H


class Light {

public:
    Light() : lx(0), ly(0), lz(0), lw(0), lr(0), lg(0), lb(0) {}
    Light( double lx_, double ly_, double lz_, double lw_, double lr_, double lg_, double lb_) :
            lx(lx_), ly(ly_), lz(lz_), lw(lw_), lr(lr_), lg(lg_), lb(lb_){}

    double lx, ly, lz, lw;
    double lr, lg, lb;

};


#endif //EXAMPLE_LIGHT_H
