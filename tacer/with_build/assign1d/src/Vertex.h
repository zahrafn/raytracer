//
// Created by Zahra Forootaninia on 10/18/16.
//

#ifndef EXAMPLE_VERTEX_H
#define EXAMPLE_VERTEX_H


class Vertex {

public:
    Vertex() : x(0), y(0), z(0) {}
    Vertex( double x_, double y_, double z_) :
            x(x_), y(y_), z(z_) {}

    double x, y, z;

};


#endif //EXAMPLE_VERTEX_H
