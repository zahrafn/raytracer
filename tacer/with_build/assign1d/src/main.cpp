//
//	To run this example, pass a text file as an argument.
//	e.g. "./example ../examplefile.txt"
//

#include "Vec3f.hpp"
#include "math.h"
#include <algorithm> // std::max
#include <vector>
#include <iostream> // cout and cerr
#include <iomanip>
#include <fstream> // file streams
#include <sstream> // stringstreams
#include "Sphere.h"
#include "Shape.h"
#include "RayType.h"
#include "Color.h"
#include "Light.h"
#include "Vertex.h"
#include <random>
#include "Parser.h"
#include "PPManeger.h"




Vec3f eye;
Vec3f viewdir;
Vec3f updir;
double fovv;
const double dist = 5.0;
int width;
int height;
Color bkgcolor;
Material mtlcolor;
std::vector< Shape* > shapes;
//std::vector< Shape* > spheres;
std::vector< Light > lights;
//std::vector< Vec3f > vertices;
//std::vector< Vec3f > normals;
//std::vector< Color > texture_pixels;
//std::vector< Vec3f > texture_vertices;
int tex_heigth, tex_width ;

// number of samples for soft shadwing
const int num_sample =50;
// soft_shadow = false for hrad shadowing
bool soft_shadow = true;
float refl_limit = 0.001;
bool shadowing = true;

const int recursive_depth_limit = 3;

float eta_vacuum = 1.0;




bool cmp(const hit & p1, const hit & p2){
	return p1.dist < p2.dist;
}


// finds the intersection to possible objects and find the color for pixle
bool get_intersect_shape( RayType &ray,  int &inx_sphere, Vec3f &intersection_point, bool &shdd, int selfid,
                          hit &element){

    using namespace std;

	bool intersect = false;
	//std::vector<std::pair<int,double> >  intersect_shapes;
    std::vector< hit >  intersect_shapes;
    std::vector<std::pair<int,double> >  shape_sh;

	shdd = false;


	for (int c = 0; c < shapes.size() ; c++) {

		shapes[c]->intersect(ray, c, intersect_shapes, shape_sh);


//        if(shape->intersect){
//            intersect_shapes.push_back(shape);
//        }

	}


    //cout<<" get_intersect_shape size: "<<intersect_shapes.size()<<endl;


    if(intersect_shapes.size() == 0){

        return false;
    }


    // intersect
    //std::pair<int,double> min = *min_element(intersect_shapes.begin(), intersect_shapes.end(), cmp);
    element = *std::min_element(intersect_shapes.begin(), intersect_shapes.end(), cmp);


        intersect = true;

        //std::cout << "min t :  " << min.first << " " << min.second<<std::endl;
        double t_intersec = element.dist;
        intersection_point.x = ray.x + t_intersec * ray.dx;
        intersection_point.y = ray.y + t_intersec * ray.dy;
        intersection_point.z = ray.z + t_intersec * ray.dz;
        inx_sphere = element.id;

        Vec3f ray_dir(ray.dx, ray.dy, ray.dz);
        bool inside = element.normal.dotP(ray_dir) > small ;


        if(inside){
            element.eta = eta_vacuum;

       }




    if (shape_sh.size() > 0) {

        shdd = true;
    }


	return intersect;

}


// finding the color for a pixle
Color shade_Ray( Vec3f &intersection_point ,Shape* curr_shape ){


	Material mat;
	if(curr_shape->is_texture()){



		double  x_tex, y_tex;
        Vec3f texture_uv;

		curr_shape->texture_cord(intersection_point, texture_uv);

		x_tex = texture_uv.x * (tex_width -1);
		y_tex =texture_uv.y * (tex_heigth -1);


		int i_tex, j_tex;
		i_tex = (int)x_tex;
		j_tex = (int)y_tex;

		double alpha_tex = x_tex - i_tex;
		double beta_tex = y_tex - j_tex;


		double col_mulit_1 = (1-alpha_tex)*(1-beta_tex);
		double col_mulit_2 = (alpha_tex)*(1-beta_tex);
		double col_mulit_3 = (1-alpha_tex)*(beta_tex);
		double col_mulit_4 = (alpha_tex)*(beta_tex);


		//int tex_col_idx_1 = i_tex * tex_heigth + j_tex;
		int tex_col_idx_1 = i_tex * tex_width +j_tex ;
		//int tex_col_idx_2 = tex_col_idx_1 + tex_heigth;
		int tex_col_idx_2 = tex_col_idx_1 + width;
		//int tex_col_idx_3 = tex_col_idx_1 + 1;
		int tex_col_idx_3 = tex_col_idx_1 + 1;
		int tex_col_idx_4 = tex_col_idx_1 + tex_width + 1;

        std::vector< Color > current_texture= curr_shape->get_texture();

		Vec3f tex_color_1  = Vec3f( current_texture[tex_col_idx_1].r, current_texture[tex_col_idx_1].g, current_texture[tex_col_idx_1].b);
		Vec3f tex_color_2  = Vec3f( current_texture[tex_col_idx_2].r, current_texture[tex_col_idx_2].g, current_texture[tex_col_idx_2].b);
		Vec3f tex_color_3  = Vec3f( current_texture[tex_col_idx_3].r, current_texture[tex_col_idx_3].g, current_texture[tex_col_idx_3].b);
		Vec3f tex_color_4  = Vec3f( current_texture[tex_col_idx_4].r, current_texture[tex_col_idx_4].g, current_texture[tex_col_idx_4].b);


		Vec3f col_vec_1;
		Vec3f col_vec_2;
		Vec3f col_vec_3;
		Vec3f col_vec_4;

		tex_color_1.constP(col_mulit_1, col_vec_1);
		tex_color_2.constP(col_mulit_2, col_vec_2);
		tex_color_3.constP(col_mulit_3, col_vec_3);
		tex_color_4.constP(col_mulit_4, col_vec_4);

		Vec3f col_sum_12;
		Vec3f col_sum_123;
		Vec3f tex_color;

		tex_color_1.add(tex_color_2,col_sum_12);
		col_sum_12.add(tex_color_3,col_sum_123);
		col_sum_123.add(tex_color_4,tex_color);


		mat = Material(Color(tex_color.x, tex_color.y, tex_color.z));

	}
	else {
		mat = curr_shape->get_matrial();
	}

	//finding the shape normal
	Vec3f normal ;

    curr_shape->get_normal(intersection_point, normal);


    //finding light vectors
	Vec3f sum;
	for (int i = 0; i < lights.size() ; i++) {

		Vec3f light_vec;
		Vec3f light(lights[i].lx, lights[i].ly, lights[i].lz);

		// point light
		if(lights[i].lw == 1) {
			light.subtract(intersection_point, light_vec);

		}
		// directional light
		else{

			light.constP(-1,light_vec);

		}
		light_vec.normalize();


		Vec3f half_vec;
		light_vec.subtract(viewdir, half_vec);
		half_vec.normalize();


		Vec3f seg1b;
		Vec3f seg2b;
		Vec3f seg3;
		Vec3f fin_seg;
		double seg1a;
		double seg2a;

		//kd Od
		Vec3f kod(mat.diffuse.r, mat.diffuse.g, mat.diffuse.b);
		kod.constP(mat.k_d,kod);


		//ks Os
		Vec3f kos(mat.specular.r, mat.specular.g, mat.specular.b);
		kos.constP(mat.k_s,kos);


		// N . L
		seg1a = normal.dotP(light_vec);
        seg1a = std::max(seg1a, 0.0);


		// N .H
		seg2a = normal.dotP(half_vec);

		seg2a = std::max(seg2a, 0.0);
		seg2a = pow(seg2a,mat.specular_highlight);




		// kod (N . L)
		kod.constP(seg1a, seg1b);
		// kos (N . H)
		kos.constP(seg2a, seg2b);
		seg1b.add(seg2b, seg3);

		//find shadows
		RayType shadow_ray;
		shadow_ray.x = intersection_point.x;
		shadow_ray.y = intersection_point.y;
		shadow_ray.z = intersection_point.z;


		// applying soft shadowing
        double shadow = 1.0;

        if(shadowing) {
            if (soft_shadow) {
                float shadow;
                double r_x, r_y, r_z;
                float sum_shad = 0.0;
                float avr_shadow;

                for (int j = 0; j < num_sample; j++) {

                    r_x = 0.01 * ((double) rand() / (RAND_MAX));
                    r_y = 0.01 * ((double) rand() / (RAND_MAX));
                    r_z = 0.01 * ((double) rand() / (RAND_MAX));


                    if (light_vec.x != 0) {
                        light_vec.x = light_vec.x + r_x;
                    }
                    if (light_vec.y != 0) {
                        light_vec.y = light_vec.y + r_y;
                    }
                    if (light_vec.z != 0) {
                        light_vec.z = light_vec.z + r_z;
                    }


                    Vec3f shadow_ray_dir;
                    shadow_ray_dir.setVec(light_vec);


                    shadow_ray.dx = shadow_ray_dir.x;
                    shadow_ray.dy = shadow_ray_dir.y;
                    shadow_ray.dz = shadow_ray_dir.z;

                    int inx_spheree = -1;
                    Vec3f sh_intersection_point;
                    bool shd = false;
                    hit hit_point;

                    bool intersect2 = get_intersect_shape(shadow_ray, inx_spheree, sh_intersection_point, shd,
                                                          curr_shape->get_matrial().refraction_index,hit_point );


                    shadow = 1.0;
                    if (shd) {

                        shadow = 0.;
                    }


                    sum_shad = sum_shad + shadow;


                }
                avr_shadow = sum_shad / ((float) num_sample);

                fin_seg.x = lights[i].lr * seg3.x * avr_shadow;
                fin_seg.y = lights[i].lg * seg3.y * avr_shadow;
                fin_seg.z = lights[i].lb * seg3.z * avr_shadow;


                sum.add(fin_seg, sum);
            }

                //hard shadowing
            else {

                Vec3f shadow_ray_dir;
                shadow_ray_dir.setVec(light_vec);


                shadow_ray.dx = shadow_ray_dir.x;
                shadow_ray.dy = shadow_ray_dir.y;
                shadow_ray.dz = shadow_ray_dir.z;

                int inx_spheree = -1;
                Vec3f sh_intersection_point;
                bool shd = false;
                hit hit_point;


                bool intersect2 = get_intersect_shape(shadow_ray, inx_spheree, sh_intersection_point, shd,
                                                      curr_shape->get_matrial().refraction_index,hit_point);



                double shadow = 1.0;
                if (shd) {

                    shadow = 0.;
                }
            }
        }


        fin_seg.x = lights[i].lr * seg3.x * shadow;
			fin_seg.y = lights[i].lg * seg3.y * shadow;
			fin_seg.z = lights[i].lb * seg3.z * shadow;


			sum.add(fin_seg, sum);

    }


	//ka Od
	Vec3f kaod(mat.ambient.r, mat.ambient.g, mat.ambient.b );
	kaod.constP(mat.k_a, kaod);

	Vec3f color_vec;
	kaod.add(sum, color_vec);




	Color curr_color;
	curr_color.r = color_vec.x;
	curr_color.g = color_vec.y;
	curr_color.b = color_vec.z;


	return curr_color;

}


// finds the intersection to possible objects and find the color for pixle
bool trace_Ray( RayType ray, Color &curr_color,  int depth, int id){

	//Color curr_color;


	Vec3f intersection_point;
	int inx_shape = -1;
	bool sh = false;
    hit hit_point;

    bool intersect = get_intersect_shape( ray, inx_shape, intersection_point, sh, id, hit_point);

    if(!intersect){
        //std::cout<<"Hit Background at depth of "<<depth<<std::endl;
        curr_color = bkgcolor;
        return false;
    }
    //transparency

// We have interseted

    //reflection
    Vec3f org_ray(ray.x, ray.y, ray.z);
    Color obj_color;

    obj_color = shade_Ray( intersection_point,  shapes[inx_shape] );



    // ####################### add specular reflection
    Vec3f normal, insident_vec,  reflection, transmitted;

    shapes[inx_shape]->get_normal(intersection_point, normal);

    intersection_point.subtract(org_ray, insident_vec );
    insident_vec.normalize();
    insident_vec.constP(-1, insident_vec);

    double NdotI = std::max(0.0, insident_vec.dotP(normal));

    Vec3f mid_norm;

    normal.constP(2.* NdotI,mid_norm);
    mid_norm.subtract(insident_vec, reflection);

    //std::cout << "reflection_ray_dir " << reflection_ray.dx<< " " << reflection_ray.dy << " "<< reflection_ray.dz << std::endl;


    double cost = NdotI;

    //reflectance_coef = pow(reflectance_coef, depth);

    // ################ add transmition

    float eta_i = hit_point.eta;
    float eta_t = ray.eta;;

    float eta_r = eta_i/eta_t; // eta_i -> hit_point.eta , eta_t -> ray.eta

    float r_0 = pow ((eta_t - eta_i) / (eta_t + eta_i) , 2);
    float reflectance_coef = r_0 +(1. - r_0) * (pow ((1. - cost), 5) );


    Vec3f first_portion, second_portion;
    float temp1 = pow(eta_r, 2);
    float temp2 = 1- pow(NdotI ,2);
    float temp3 = sqrt( std::max(1e-5, 1.0 - (temp1 * temp2)));

    normal.constP(-temp3, first_portion);

    //normal.constP( (- std::sqrt(1 - (pow(eta_r, 2) )*(1- pow(NdotI ,2) ) )), first_portion );


    normal.constP(NdotI, second_portion);
    second_portion.subtract(insident_vec, second_portion);
    second_portion.constP(eta_r, second_portion);


    first_portion.add(second_portion, transmitted);


    Color reflected_color;
    Color transmitted_color;



    if (depth < recursive_depth_limit ){

        RayType reflection_ray(intersection_point, reflection, 1);
        RayType transmitted_ray(intersection_point, transmitted, hit_point.eta);


        // recursive part for transparency and reflection
        trace_Ray( reflection_ray, reflected_color, depth+1, shapes[inx_shape]->id);
        trace_Ray( transmitted_ray, transmitted_color, depth+1, shapes[inx_shape]->id);



        // Trace Refracter ray

        float opacity = shapes[inx_shape]->get_matrial().opacity;


        curr_color.r = obj_color.r * (1-reflectance_coef) + (reflected_color.r * reflectance_coef ) * opacity + transmitted_color.r  * (1. - opacity );
        curr_color.g = obj_color.g * (1-reflectance_coef) + (reflected_color.g * reflectance_coef ) * opacity+ transmitted_color.g * (1. - opacity );
        curr_color.b = obj_color.b * (1-reflectance_coef) + (reflected_color.b * reflectance_coef ) * opacity+ transmitted_color.b  * (1. - opacity );


//        curr_color.r = obj_color.r * (1-reflectance_coef) + (reflected_color.r  * opacity + transmitted_color.r  * (1. - opacity )  * reflectance_coef);
//        curr_color.g = obj_color.g * (1-reflectance_coef) + (reflected_color.g  * opacity+ transmitted_color.g * (1. - opacity )* reflectance_coef);
//        curr_color.b = obj_color.b * (1-reflectance_coef) + (reflected_color.b   * opacity+ transmitted_color.b  * (1. - opacity )* reflectance_coef);




//        std::cout << "######## shape id "<< inx_shape<< std::endl;
//        std::cout << "reflection_ray_dir " << reflection_ray.dx<< " " << reflection_ray.dy << " "<< reflection_ray.dz << std::endl;
//        std::cout << "transmitted_ray " << transmitted_ray.dx<< " " << transmitted_ray.dy << " "<< transmitted_ray.dz << std::endl;
//
//        std::cout << "reflected_color " << reflected_color.r<< " " << reflected_color.g << " "<< reflected_color.b << std::endl;
//        std::cout << "transmitted_color " << transmitted_color.r<< " " << transmitted_color.g << " "<< transmitted_color.b << std::endl;
//
//        std::cout << "obj_color " << obj_color.r<< " " << obj_color.g << " "<< obj_color.b << std::endl;



    }
    else {

        curr_color = obj_color;
    }

    // clamp the color
    if(curr_color.r >1){
        curr_color.r = 1;
    }
    if(curr_color.g >1){
        curr_color.g = 1;
    }
    if(curr_color.b >1){
        curr_color.b = 1;
    }

    if(curr_color.r < 0){
        curr_color.r = 0;
    }
    if(curr_color.g <0){
        curr_color.g = 0;
    }
    if(curr_color.b < 0){
        curr_color.b = 0;
    }

    //std::cout << "final color " << curr_color.r<< " " << curr_color.g << " "<< curr_color.b << std::endl;


    return true;


}





// In main:
// - I read the input file and find the viewing window locations,
// -shoot the ray from each pixel in the image
// - find in the ray has intersection with the shape using and get the color for each pixel usnig trace_Ray method
//- the make the image using makePPM method
int main( int argc, char **argv ){


	using namespace std;


	// Make sure the user specified an input file. If not, tell them how to do so.
	if( argc < 2 ){ std::cerr << "**Error: you must specify an input file, " <<
		"e.g. \"./example ../examplefile.txt\"" << std::endl; return 0; }

	// We'll store the list of objects parsed from the text file in std::vectors.

	// Get the input text file, which should be the second argument.
	std::string filename( argv[1] );
	std::cout << "######### Input file: " << filename << std::endl;

	Parser parser;
	parser.parse_file(filename);

	eye = parser.eye;
	viewdir = parser.viewdir;
	updir = parser.updir;
	fovv = parser.fovv;
	bkgcolor = parser.bkgcolor;
	mtlcolor = parser.mtlcolor;
	shapes = parser.shapes;
	lights = parser.lights;
	width = parser.width;
	height = parser.height;
	tex_heigth = parser.tex_heigth;
	tex_width = parser.tex_width;

    //return 0;


	std::cout << "#################### "<< std::endl;

	Vec3f u;
	Vec3f v;
	Vec3f n;

	// window size
	double h;
	double w;



	// find u and v vectors

	viewdir.crossP(updir, u);
	u.normalize();

	u.crossP(viewdir, v);
	v.normalize();

	n.setVec(viewdir);
	n.normalize();

	// center of window
	Vec3f q;
	n.constP(dist , q);
	Vec3f p ;
	eye.add( q , p);

	std::cout << "u: " << u.x << " " << u.y << " "<< u.z << std::endl;
	std::cout << "v: " << v.x << " " << v.y << " "<< v.z << std::endl;

	//find the window size

	h = 2*dist * tan( (fovv/2.) * M_PI / 180.);
	float asp_r = (float)width / (float)height;
	w = h * asp_r;


	// 3D coordinates of corners of the viewing window
	Vec3f dv, du;
	v.constP(h/2. ,dv);
	u.constP(w/2, du);


	Vec3f ul, ur, ll, lr;
	Vec3f mu, sub1, sub2 , fin;

	// set ul
	dv.subtract(du,sub1);
	p.add( sub1, fin);
	ul.setVec(fin);

	// set ur
	dv.add( du , sub2);
	p.add( sub2, fin);
	ur.setVec(fin);

	// set ll
	dv.add( du , sub2);
	p.subtract( sub2, fin);
	ll.setVec(fin);

	// set lr
	du.subtract(dv,sub1);
	p.add( sub1, fin);
	lr.setVec(fin);

	std::cout << "############# viewing window ################"<< std::endl;
	std::cout << "ul: " << ul.x << " " << ul.y << " "<< ul.z  << std::endl;
	std::cout << "ur: " << ur.x << " " << ur.y << " "<< ur.z  << std::endl;
	std::cout << "ll: " << ll.x << " " << ll.y << " "<< ll.z  << std::endl;
	std::cout << "lr: " << lr.x << " " << lr.y << " "<< lr.z  << std::endl;
	std::cout << "#############################################"<< std::endl;



	// horizental offset and vertical offcet
	Vec3f del_h, del_v;
	Vec3f numin , last;

	ur.subtract(ul, numin);
	numin.constP( 1./ (width - 1.) , last );
	del_h.setVec(last);

	ll.subtract(ul, numin);
	numin.constP(1./ (height - 1.), last);
	del_v.setVec(last);

	std::cout << "del_h: " << del_h.x << " " << del_h.y << " "<< del_h.z  << std::endl;
	std::cout << "del_v: " << del_v.x << " " << del_v.y << " "<< del_v.z  << std::endl;

	std::vector<Color> pixels;


	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {


			// find the offests to find positions of pixels
			Vec3f pix;
			Vec3f seg1, seg2, seg3;
			del_v.constP((double) i, seg1);
			del_h.constP((double) j, seg2);
			seg1.add( seg2, seg3);
			ul.add( seg3, seg3);
			pix.setVec(seg3);

			// making a ray
			Vec3f raydir;
			Vec3f res;
			pix.subtract(eye, res);
			raydir.setVec(res);
			raydir.normalize();


			RayType ray;
			ray.x = eye.x, ray.y = eye.y, ray.z = eye.z;

			ray.dx = raydir.x, ray.dy = raydir.y, ray.dz = raydir.z;

            ray.eta = eta_vacuum;

			// finding quatradic multipliers a, b and c
			Color curr_color;
            //std::cout << " ################# Pix " << i <<" , "<< j << std::endl;
           //std::cout << "ray: " << ray.x << " " << ray.y<< " "<< ray.z  <<" dir: " << ray.dx << " " << ray.dy<< " "<< ray.dz  << std::endl;


            trace_Ray( ray, curr_color, 0, -1);
			//std::cout << "curr_color: " << curr_color.r << " " << curr_color.g<< " "<< curr_color.b  << std::endl;


			pixels.push_back(curr_color);


		}
	}

	PPManeger ppmmaker;

	ppmmaker.makePPM(filename,pixels, width, height);


	// All done.
	return 0;


} // end main

//
int main_test( int argc, char **argv ){

    if( argc < 2 ){ std::cerr << "**Error: you must specify an input file, " <<
                    "e.g. \"./example ../examplefile.txt\"" << std::endl; return 0; }

    // We'll store the list of objects parsed from the text file in std::vectors.

    // Get the input text file, which should be the second argument.
    std::string filename( argv[1] );
    std::cout << "######### Input file: " << filename << std::endl;

    Parser parser;
    parser.parse_file(filename);

    eye = parser.eye;
    viewdir = parser.viewdir;
    updir = parser.updir;
    fovv = parser.fovv;
    bkgcolor = parser.bkgcolor;
    mtlcolor = parser.mtlcolor;
    shapes = parser.shapes;
    lights = parser.lights;
    width = parser.width;
    height = parser.height;
    tex_heigth = parser.tex_heigth;
    tex_width = parser.tex_width;


    float mat_prop[12] ={1., 0., 0., 0., 0., 1.0, 0.2, 0.2, 0., 10.0, 0.5, 4.1};



    Vec3f ray_pos(0.8, 0, 8);
    Vec3f ray_dir(0,0 , -1);

    RayType ray(ray_pos, ray_dir, eta_vacuum);
    //RayType ray(eye, viewdir, eta_vacuum);

    Color color;

    trace_Ray(ray, color, 0, -1);

    //std::cout << "final color " << color.r <<" " << color.g << " " << color.b << std::endl;


return 0;


}

