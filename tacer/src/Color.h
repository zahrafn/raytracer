//
// Created by Zahra Forootaninia on 9/21/16.
//

#ifndef EXAMPLE_COLOR_H
#define EXAMPLE_COLOR_H

#include <math.h>
#include <iostream>

class Color{
public:

    float r;
    float g;
    float b;


    Color() : r(0), g(0), b(0) {}

    Color( float r_, float g_, float b_) : r(r_), g(g_), b(b_){

    };

    Color(float rgb_[]) : r(rgb_[0]), g(rgb_[1]), b(rgb_[2])  {};



};


class Material{

public:

    float k_d =0.0;
    Color diffuse;

    float k_s = 0.;
    Color specular;

    float k_a = 0.0f;
    Color ambient;

    float specular_highlight = 1.0f;

    float opacity = 0.0f;
    float refraction_index = 1.0f;
    int id = 0;



//
//    float specular_;
//    float diffuse_;
//    float ambient_

    Material() : diffuse(), specular(), ambient(), specular_highlight(), id(0) {}

    Material(Color diffuse_) : diffuse(diffuse_), k_d(1.0) {}

    Material(float material[], int id_)
            : diffuse(&material[0]),k_d(material[7]),
              specular(&material[3]),k_s(material[8]) ,
              ambient(&material[0]), k_a(material[6]),
              specular_highlight(material[9]), opacity(material[10]), refraction_index(material[11]), id(id_) {


//        std::cout << "############################################ " << std::endl;
//
//        std::cout << "diffuse " << diffuse.r << " "<< diffuse.g <<" "<<diffuse.b<<std::endl;
//        std::cout << "diffuse K " << k_d << std::endl;
//
//        std::cout << "specular " << specular.r << " "<< specular.g <<" "<<specular.b<<std::endl;
//        std::cout << "specular K " << k_s << std::endl;
//
//        std::cout << "ambient " << ambient.r << " "<< ambient.g <<" "<<ambient.b<<std::endl;
//        std::cout << "ambient K " << k_a << std::endl;
//        std::cout << "############################################ " << std::endl;




    }





};








//class Color {
//public:
//    Color() : od_r(0), od_g(0), od_b(0), os_r(0), os_g(0),os_b(0), ka(0), kd(0), ks(0), n_c(0), m(0)  {}
//
//
//    // r g b are od_r_, od_r_
//    Color( double od_r_, double od_g_, double od_b_, double os_r_, double os_g_, double os_b_,
//           double ka_, double kd_, double ks_, double n_c_, int m_) :
//            od_r(od_r_), od_g(od_g_), od_b(od_b_),os_r(os_r_), os_g(os_g_),os_b(os_b_), ka(ka_), kd(kd_), ks(ks_), n_c(n_c_), m(m_) {}
//
//
//    /**
//     *
//     * Set R G B color.
//     *
//     */
//    Color(double r, double g, double b):
//        od_r(r), od_g(g), od_b(b) {}
//
//
//    double od_r, od_g, od_b;
//    double os_r, os_g, os_b;
//    double ka, kd, ks, n_c;
//    int m;
//
//};


#endif //EXAMPLE_COLOR_H
