//
// Created by Zahra Forootaninia on 10/29/16.
//

#ifndef EXAMPLE_PARSER_H
#define EXAMPLE_PARSER_H

#include <string>
#include <memory>

#include "Color.h"
#include "Shape.h"
#include "PPManeger.h"




class Parser {

public:

    Vec3f eye;
    Vec3f viewdir;
    Vec3f updir;
    double fovv;
    const double dist = 5.0;
    int width;
    int height;
    Color bkgcolor;
    Material mtlcolor;
    std::vector< Shape* > shapes;
    std::vector< Light > lights;
    std::vector< Vec3f > vertices;
    std::vector< Vec3f > normals;
    std::vector< Vec3f > texture_vertices;
    std::vector< Color > texture_pixels;
    int tex_heigth, tex_width ;




    bool parse_args_int(int size, const std::vector<std::string>& inputs , int array_out[]) {

        using namespace std;


        if (inputs.size() < size +1) {
            cerr << "**Error: not enough inputs for " << inputs[0] << endl;
            return 0;
        }
        cout << inputs[0]<< ": ";
        for (int i = 1; i <= size; i++) {
            array_out[i-1] = atoi(inputs[i].c_str()) ;
            cout << array_out[i-1] << " ";
        }

        cout << endl;

        return true;
    }

    bool parse_args_float(int size, const std::vector<std::string>& inps , float array_out[]) {

        using namespace std;

        //std::cerr <<"inps size " <<inps.size() << std::endl;

        if (inps.size() < size +1) {
            cerr << "**Error: not enough inputs for " << inps[0] << endl;
            return false;
        }


        cout << inps[0]<< ": ";
        for (int i = 1; i <= size; i++) {
            array_out[i-1] = stof(inps[i]) ;
            cout << array_out[i-1] << " ";
        }

        cout << endl;

        return true;
    }

    bool parse_args_str(int size, const std::vector<std::string>& inps , std::string array_out[]) {

        using namespace std;

        //cout << inps[0]<< ":lolllll "<< inps[0] << " "<< inps[1]<< " "<< inps[3]<<endl;



        if (inps.size() < size +1) {
            cerr << "**Error: not enough inputs for " << inps[0] << endl;
            return 0;
        }
        cout << inps[0]<< ": ";
        for (int i = 1; i <= size; i++) {
            array_out[i-1] = inps[i] ;
            cout << array_out[i-1] << " ";
        }

        cout << endl;

        return true;
    }

    void split_by_string(std::string divider,  std::string str_list[3], int offset, int inx1[3], int inx2[3] ){

        std::string f0,f1;


        for(int i=0; i< 3; i++) {
            f0 = str_list[i].substr(0, str_list[i].find(divider));
            f1 = str_list[i].substr(str_list[i].find(divider) + offset, str_list[i].size() - 1);
            inx1[i] = atoi(f0.c_str());
            inx2[i] = atoi(f1.c_str());
        }


    }


    bool parse_file(std::string filename) {

        using namespace std;

        float eye_init[3];
        float viewdir_init[3];
        float updir_init[3];
        float fov_init[1];
        int imagesize_init[2];
        float  bkgcolor_init[3];
        float mtlcolor_init[12];

        float sphere_init[4];
        float light_init[7];
        float v_init[3];
        float vn_init[3];
        int vt_init[3];
        std::string f_init[3];

        float clr_inx = 0;
        int shape_id = 0;
        int tx_inx = 0;

        bool norm_flag;
        bool isTexture;




        // Create a filestream for reading the text file, and open it.
        // The is_open call will return false if there was a problem.
        ifstream inputfile(filename.c_str());
        if (inputfile.is_open()) {


            // Use std::getline to parse the text file one line at a time.
            string line;
            while (std::getline(inputfile, line)) {


                if((line.at(0) == '#') || (line.size() == 0)){
                    continue;
                }


                //std::stringstream ss(line);

                stringstream stream(line);
                string token;
                vector <std::string> inputs;

                while (std::getline(stream, token, ' ')) {
                    inputs.push_back(token);
                }


                string var = "";
                var = inputs[0];
                //std::cerr << "ss "<< ss.str() << std::endl;



                if (var == "eye") {
                    parse_args_float(3, inputs, eye_init);
                    eye = Vec3f(eye_init[0], eye_init[1], eye_init[2]);
                }

                else if (var == "viewdir") {
                    parse_args_float(3, inputs, viewdir_init);
                    viewdir = Vec3f(viewdir_init[0], viewdir_init[1], viewdir_init[2]);


                }
                else if (var == "updir") {
                    parse_args_float(3,inputs , updir_init);
                    updir = Vec3f(updir_init[0], updir_init[1], updir_init[2]);


                }

                else if( var == "fovv" ) {
                    parse_args_float(1,inputs , fov_init);
                    fovv = fov_init[0];

                    if(fovv >= 180 || fovv ==0 ){
                        std::cerr << "**Error: field of view should be between 0 and 180  " << std::endl; return 0;
                    }
                }
                else if( var == "imsize" ) {

                    parse_args_int(2,inputs , imagesize_init);
                    width =imagesize_init[0];
                    height= imagesize_init[1];


                    if(width <= 0 || height <=0){
                        std::cerr << "**Error: width and height shoul be a possitive int " << std::endl; return 0;
                    }
                }
                else if( var == "bkgcolor" ) {

                    parse_args_float(3,inputs , bkgcolor_init);
                    bkgcolor = Color(bkgcolor_init[0], bkgcolor_init[1], bkgcolor_init[2]);

                }

                else if( var == "mtlcolor" ) {

                    mtlcolor_init[10] = 1.0;
                    mtlcolor_init[11] = 1.01;

                    parse_args_float(12,inputs , mtlcolor_init);
                    mtlcolor = Material(mtlcolor_init,clr_inx );
                    isTexture = false;

                    clr_inx = clr_inx +1;

                }
                else if( var == "texture" ){

                    isTexture = true;

                    if (inputs.size() < 2 ){
                        std::cerr << "**Error: not enough inputs for " << var << std::endl; return 0;
                    }



                    std::string texture_file ="";
                    texture_file = inputs[1];

                    PPManeger texture_reader;
                    texture_reader.readPPM(texture_file , texture_pixels );
                    tex_heigth = texture_reader.height_outp;
                    tex_width = texture_reader.width_outp;

                    tx_inx = tx_inx +1;

                    std::cout << "texture : " << texture_file << std::endl ;



                }
                else if( var == "sphere" ) {

                    parse_args_float(4,inputs , sphere_init);

                    Material mat;
                    std::vector< Color > curr_texture;


                    if(!isTexture){
                        mat = mtlcolor;
                    }
                    else{
                        curr_texture = texture_pixels;
                    }

                    shapes.push_back(new Sphere(sphere_init[0], sphere_init[1], sphere_init[2], sphere_init[3],isTexture, mat, curr_texture, shape_id++));

                }
                else if( var == "light" ) {
                    parse_args_float(7,inputs , light_init);
                    lights.push_back(Light(light_init[0], light_init[1], light_init[2],light_init[3],light_init[4],light_init[5],light_init[6] ));


                }

                else if ( var == "v" ) {
                    parse_args_float(3,inputs , v_init);

                    vertices.push_back( Vec3f(v_init[0] , v_init[1], v_init[2]) );

                }

                else if ( var == "vn" ){
                    parse_args_float(3,inputs , vn_init);

                    if(vn_init[0] +vn_init[1] +vn_init[2]>0){
                        norm_flag = true;
                    }
                    normals.push_back( Vec3f(vn_init[0] , vn_init[1], vn_init[2]) );


                }

                else if ( var == "f" ) {

                    parse_args_str(3,inputs , f_init);


                    int fv[3];
                    int fn[3];
                    int fvt[3];

                    bool two_sl = f_init[0].find("//") != std::string::npos;
                    bool one_sl = f_init[0].find("/") != std::string::npos;

                    if(two_sl) {

                        split_by_string("//", f_init , 2 , fv, fn );

                    }
                    else if(one_sl){

                        split_by_string("/", f_init , 1 , fv, fvt );
                        std::cout<< "fv "<<fv[0]<< " " <<fv[1]<<" " << fv[2]<< std::endl;
                        std::cout<< "fvt "<<fvt[0]<< " " <<fvt[1]<<" " << fvt[2]<< std::endl;


                    }
                    else{


                        fv[0] = atoi(f_init[0].c_str());
                        fv[1] = atoi(f_init[1].c_str());
                        fv[2] = atoi(f_init[2].c_str());
                    }


                    Material mat;
                    std::vector< Color > curr_texture;


                    if(!isTexture){
                        mat = mtlcolor;
                    }
                    else{
                        curr_texture = texture_pixels;
                    }


                    // case we have normals
                    if(norm_flag) {

                        Triangle *t = new Triangle(fv[0], fv[1], fv[2], vertices[fv[0] - 1], vertices[fv[1] - 1], vertices[fv[2] - 1],
                                                   normals[fn[0] - 1], normals[fn[1] - 1], normals[fn[2] - 1],shape_id++);

                        t->set_texture_bool(isTexture);
                        shapes.push_back(t);

                        if(!isTexture) {

                            t->set_matrial(mat);

                        }
                        else{

                            t->set_texture_vertices(texture_vertices[fvt[0] - 1], texture_vertices[fvt[1] - 1], texture_vertices[fvt[2] - 1]);
                            t->set_texture(curr_texture);
                        }
                    }
                        // case we don't have normals

                    else{

                        Triangle *t = new Triangle(fv[0], fv[1], fv[2], vertices[fv[0] - 1], vertices[fv[1] - 1], vertices[fv[2] - 1]
                                ,shape_id++);
                        shapes.push_back(t);
                        t->set_texture_bool(isTexture);

                        if(!isTexture){

                            t->set_matrial(mat);
                        }
                        else {

                            t->set_texture_vertices(texture_vertices[fvt[0] - 1], texture_vertices[fvt[1] - 1], texture_vertices[fvt[2] - 1]);
                            t->set_texture(curr_texture);
                        }

                    }


                }

                else if(var == "vt"){

                    parse_args_int(3,inputs , vt_init);
                    texture_vertices.push_back(Vec3f(vt_init[0], vt_init[1],vt_init[2]));



                }
//                    // If it's a comment, don't do anything.
//                else if( var == "#" ){}
//                else if(var == ""){}

                    // Otherwise, it's an error!
                else { std::cerr << "**Error: I don't know what \"" << var << "\" is! " << std::endl; }

            } // end parse line

            // check if updir and viewdir are parallel
            Vec3f updir_x_viewdir;
            updir.crossP(viewdir ,updir_x_viewdir );
            double updir_x_viewdir_mag = sqrt(updir_x_viewdir.x* updir_x_viewdir.x + updir_x_viewdir.y* updir_x_viewdir.y +updir_x_viewdir.z* updir_x_viewdir.z);
            if(updir_x_viewdir_mag == 0){
                std::cerr << "**Error: updir and viewdir are parallel " <<  std::endl; return 0;
            }



        }

        else { std::cerr << "**Error: Could not open file " << filename << std::endl; return 0; }


        return true;


    }


};




#endif //EXAMPLE_PARSER_H
