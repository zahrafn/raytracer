//
// Created by Zahra Forootaninia on 10/30/16.
//

#ifndef EXAMPLE_PPMANEGER_H
#define EXAMPLE_PPMANEGER_H


#include "Vec3f.hpp"
#include "math.h"
#include <algorithm> // std::max
#include <vector>
#include <iostream> // cout and cerr
#include <iomanip>
#include <fstream> // file streams
#include <sstream> // stringstreams
#include "Color.h"


class PPManeger {

public:

    std::string ppmfileName;
    std::vector<Color> pixels; // this can be pixles or texture
    int height_outp, width_outp ;

    PPManeger() : ppmfileName(""), pixels(0) { }

    void makePPM(std::string fileName,std::vector<Color> pixels, int width_pic, int height_pic ){



        int pos = fileName.find("txt");
        if (pos != std::string::npos) {
            fileName.replace(pos, 3, "ppm");   // 5 = length( $name )
        }

        std::ofstream output(fileName.c_str());

        output << "P3" << std::endl;
        output << width_pic << " " << height_pic << std::endl;
        output << "255" << std::endl;

        int totalSize = width_pic * height_pic;

        if (output.is_open()) {

            for (int i = 0; i < totalSize; i++) {
                pixels[i].r = pixels[i].r  * 255.;
                pixels[i].g = pixels[i].g  * 255.;
                pixels[i].b = pixels[i].b  * 255.;

                output << (int)pixels[i].r << " ";
                output << (int)pixels[i].g << " ";
                output << (int)pixels[i].b << std::endl;
            }

            output.close();
        }
        else std::cout << "!!!ERROR:Unable to outup file \n";


        std::cout << "Successfuly output .ppm file \n";

    }


    void readPPM(std::string fileName,std::vector<Color> &texture){



        std::ifstream inputfile( fileName.c_str() );
        if( inputfile.is_open() ) {


            // Use std::getline to parse the text file one line at a time.
            std::string firstline = "";
            std::getline(inputfile, firstline);
            std::string wh = firstline.substr(3, firstline.length()-1);

            std::string w = wh.substr(0, wh.find(" "));
            std::string wh_cut = wh.substr(wh.find(" ")+1, wh.length()-1);

            std::string h = wh_cut.substr(0, wh_cut.find(" "));

            height_outp = atoi(h.c_str());
            width_outp = atoi(w.c_str());
            std::cout << "texture size: " << height_outp << " " << width_outp << " " <<std::endl;




            std::string line;


            for (int i = 0; i < height_outp; i++) {
                for (int j = 0; j < width_outp; j++) {


                    std::getline(inputfile, line);

                    std::stringstream ss(line);

                    std::stringstream stream(line);
                    std::string token;
                    std::vector<std::string> inps;

                    while (std::getline(stream, token, ' ')) {
                        inps.push_back(token);
                    }

                    double texture_r, texture_g, texture_b;
                    ss >> texture_r >> texture_g >> texture_b;
                    //std::cout << " texture "<< texture_r<<" " <<texture_g<<" " <<texture_b<< std::endl;
                    texture.push_back(Color(texture_r, texture_g, texture_b));

                }
            }

        }


        else std::cout << "!!!ERROR:Unable to read PPM file \n";



    }

};


#endif //EXAMPLE_PPMANEGER_H
